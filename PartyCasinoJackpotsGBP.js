export const data = {
  "PartyCasino Jackpots json feed": [
    {
      jackpotName: "Hot Hot Jackpot",
      jackpotValue: "50171.34 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Hot Hot Jackpot",
          subjackpotValue: "50171.34 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Raffle Jackpot",
      jackpotValue: "0.12 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Raffle Jackpot",
          subjackpotValue: "0.12 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Fluffy Mega Jackpot",
      jackpotValue: "8094.95 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Fluffy Mega Jackpot",
          subjackpotValue: "8094.95 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Basic Instinct",
      jackpotValue: "88267.99 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Basic Instinct",
          subjackpotValue: "88267.99 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Divine Fortune Jackpot",
      jackpotValue: "35299.64 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "gof_mega",
          subjackpotValue: "35299.64 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Mega Moolah",
      jackpotValue: "7559438.26 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Mega Moolah",
          subjackpotValue: "7559438.26 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Cosmic Fortune Jackpot",
      jackpotValue: "64109.81 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "cf_mega",
          subjackpotValue: "44549.32 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "cf_rapidfix",
          subjackpotValue: "87.72 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "cf_minifix",
          subjackpotValue: "438.64 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "cf_midi",
          subjackpotValue: "1582.59 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "cf_major",
          subjackpotValue: "17451.53 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Casino Holdem Jumbo7 Jackpot",
      jackpotValue: "1778860.81 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Casino Holdem Jumbo7 Jackpot",
          subjackpotValue: "1778860.81 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "BP JACKPOT KING",
      jackpotValue: "377510.95 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "JACKPOT KING",
          subjackpotValue: "377510.95 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "BP Genie Jackpots",
      jackpotValue: "0.00 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Genie Jackpots",
          subjackpotValue: "0.00 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Caribbean Jackpot",
      jackpotValue: "24872.54 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "cstud",
          subjackpotValue: "24872.54 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Gift Of Wealth",
      jackpotValue: "83848.04 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "bof_mega",
          subjackpotValue: "83848.04 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "IGT Mega Jackpot",
      jackpotValue: "674254.48 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "IGT Mega Jackpot",
          subjackpotValue: "674254.48 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Gold Jackpots",
      jackpotValue: "466443.83 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Gold Mega Jackpot",
          subjackpotValue: "404700.84 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Gold Super Jackpot",
          subjackpotValue: "48820.45 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Gold Quick Jackpot",
          subjackpotValue: "12300.83 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Gold Blitz Jackpot",
          subjackpotValue: "621.71 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "PIZZAJACKPOTTEN Jackpot",
      jackpotValue: "10117.05 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "PIZZAJACKPOTTEN MEDIUM Jackpot",
          subjackpotValue: "134.48 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "PIZZAJACKPOTTEN LARGE Jackpot",
          subjackpotValue: "706.88 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "PIZZAJACKPOTTEN X-LARGE Jackpot",
          subjackpotValue: "9275.69 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Medallion Progressive Jackpot",
      jackpotValue: "888.00 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Medallion Progressive Jackpot",
          subjackpotValue: "888.00 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Ca$h Bash Jackpot",
      jackpotValue: "376216.64 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Ca$h Bash Super Jackpot",
          subjackpotValue: "1396.77 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Ca$h Bash Mega Jackpot",
          subjackpotValue: "374819.87 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Great Blue Jackpot",
      jackpotValue: "87770.12 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Great Blue Jackpot",
          subjackpotValue: "87770.12 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Caribbean Stud Poker Jackpot",
      jackpotValue: "342539.21 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Caribbean Stud Poker Jackpot",
          subjackpotValue: "342539.21 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "TERMINATORROULETTE Jackpot",
      jackpotValue: "15699.86 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "TERMINATORROULETTE T-800 Jackpot",
          subjackpotValue: "11786.29 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "TERMINATORROULETTE T-1 Jackpot",
          subjackpotValue: "3913.58 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Big Series",
      jackpotValue: "1279759.55 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "The Big One Super Cash",
          subjackpotValue: "812.40 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "The Big One Blitz Cash",
          subjackpotValue: "7.64 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "The Big One Quick Cash",
          subjackpotValue: "49.09 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "The Big One Mega Cash",
          subjackpotValue: "72033.39 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "The Big One Colossal Cash",
          subjackpotValue: "1206857.01 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "BP Super Diamond Deluxe",
      jackpotValue: "14570.70 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Super Diamond Deluxe",
          subjackpotValue: "14570.70 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Blitz Cash",
      jackpotValue: "62573.04 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Colosal Cash",
          subjackpotValue: "46915.01 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Blitz Cash",
          subjackpotValue: "16.36 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Quick Cash",
          subjackpotValue: "26.95 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Super Cash",
          subjackpotValue: "2184.73 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Mega Cash",
          subjackpotValue: "13429.99 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Mystery Cashcano MEGA",
      jackpotValue: "47829.80 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Mystery Cashcano GRAND",
          subjackpotValue: "1325.41 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Mystery Cashcano MEGA",
          subjackpotValue: "46466.39 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Mystery Cashcano MINI",
          subjackpotValue: "38.00 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Cash Flash Jackpots",
      jackpotValue: "1125.92 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Ca$h Fla$h Jackpot",
          subjackpotValue: "1125.92 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Must Go",
      jackpotValue: "186980.48 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Must Go Rapid Jackpot",
          subjackpotValue: "469.87 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Mega Jackpot",
          subjackpotValue: "180752.06 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Must Go 5k Jackpot",
          subjackpotValue: "268.06 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Daily Jackpot",
          subjackpotValue: "5490.49 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "HanzelAndGretalTreasureTrail",
      jackpotValue: "8080.05 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Silver",
          subjackpotValue: "55.48 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Gold",
          subjackpotValue: "202.59 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Ruby",
          subjackpotValue: "1067.60 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Diamond",
          subjackpotValue: "6754.37 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "BP Winstar",
      jackpotValue: "68127.68 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Winstar",
          subjackpotValue: "68127.68 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Lucky Chest Jackpot",
      jackpotValue: "219182.36 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Lucky Chest Jackpot",
          subjackpotValue: "219182.36 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "PCP Jackpot Group",
      jackpotValue: "116695.48 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Progressive Caribbean Poker Jackpot",
          subjackpotValue: "116695.48 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Diamond Wild",
      jackpotValue: "646153.61 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Diamond Wild",
          subjackpotValue: "646153.61 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Arabian Jackpot",
      jackpotValue: "927391.55 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "arabian",
          subjackpotValue: "927391.55 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Atlantis Poseidon Jackpot",
      jackpotValue: "5364.53 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Atlantis Poseidon Jackpot",
          subjackpotValue: "4486.77 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Atlantis Hercules Jackpot",
          subjackpotValue: "877.76 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Super Jackpot",
      jackpotValue: "44017.48 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Super Jackpot",
          subjackpotValue: "44017.48 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Dragon Jackpot",
      jackpotValue: "66642.28 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Dragon Jackpot",
          subjackpotValue: "66642.28 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Oktoberfest Madness Smashed Jackpot",
      jackpotValue: "8717.26 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Oktoberfest Madness Smashed Jackpot",
          subjackpotValue: "7720.79 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Oktoberfest Madness Tipsy Jackpot",
          subjackpotValue: "861.09 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Oktoberfest Madness Buzzed Jackpot",
          subjackpotValue: "135.38 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Hall of Gods Jackpot",
      jackpotValue: "1869262.03 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "hog_medium",
          subjackpotValue: "198523.84 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "hog_large",
          subjackpotValue: "1669940.84 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "hog_small",
          subjackpotValue: "797.36 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Fire Drake 2 Inferno Jackpot",
      jackpotValue: "7794.18 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Fire Drake 2 Blaze Jackpot",
          subjackpotValue: "640.66 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Fire Drake 2 Ember Jackpot",
          subjackpotValue: "116.82 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "Fire Drake 2 Inferno Jackpot",
          subjackpotValue: "7036.69 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "megajackpot",
      jackpotValue: "2238173.13 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "megajackpot2",
          subjackpotValue: "37813.13 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "megajackpot3",
          subjackpotValue: "1050.41 GBP",
          subjackpotCurrency: "GBP"
        },
        {
          subjackpotName: "megajackpot1",
          subjackpotValue: "2199309.58 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Age of the Gods Jackpot",
      jackpotValue: "361090.40 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Age of the Gods Jackpot",
          subjackpotValue: "361090.40 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Five Stars Mega Jackpot",
      jackpotValue: "12704.10 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: [
        {
          subjackpotName: "Five Stars Mega Jackpot",
          subjackpotValue: "12704.10 GBP",
          subjackpotCurrency: "GBP"
        }
      ]
    },
    {
      jackpotName: "Total",
      jackpotValue: "20236639.29 GBP",
      currency: "GBP",
      lastUpdatedDate: "02/08/19 06:01:29 ET",
      subJackpotDetails: []
    }
  ]
};
