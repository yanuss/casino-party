import { data } from "../../PartyCasinoJackpotsGBP";

const jackpotValue = data["PartyCasino Jackpots json feed"].find(
  el => el.jackpotName === "Big Series"
).jackpotValue;

const terms = document.querySelector(".expand-ico");
const collapsed = document.querySelector(".collapsed");

terms.addEventListener("click", function() {
  collapsible(this, collapsed);
});
function collapsible(that, node) {
  if (node.style.maxHeight) {
    node.style.maxHeight = null;
    collapsibleIcon(that);
  } else {
    node.style.maxHeight = node.scrollHeight + "px";
    collapsibleIcon(that);
  }
}

function collapsibleIcon(el) {
  if (el.classList.contains("expanded")) {
    el.classList.remove("expanded");
  } else {
    el.classList.add("expanded");
  }
}
hideNode(
  document.querySelector(".close-btn"),
  document.querySelector(".jackpot")
);

function hideNode(element, target) {
  element.addEventListener("click", function() {
    target.style = "display: none";
  });
}

// clock//

function parseValue(val) {
  const result = val.split(" ").filter(el => !isNaN(el));
  return result[0];
}

(function() {
  const clock = $("#jackpot-countdown").FlipClock(parseValue(jackpotValue), {
    clockFace: "Counter"
  });

  setTimeout(function() {
    setInterval(function() {
      clock.increment();
    }, 3000);
  });
})();
